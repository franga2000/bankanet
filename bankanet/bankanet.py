import pickle
import urllib
from datetime import datetime, timedelta
from typing import List
from enum import StrEnum

import pytz
from requests import Session

class BNNotAuthenticatedException(Exception):
	pass

class OrderStatus(StrEnum):
	UNCONFIRMED = "POTRD"
	EXECUTED = "IZVD"
	REJECTED = "ZAVRN"

class BankaNet():
	
	def __init__(self, url_base="https://bankanet.nkbm.si", user_agent="Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/74.0"):
		self._url_base = url_base
		self._api_base = self._url_base + "/bnk/resources/"
		self._sess = Session()
		self._headers = {
			"User-Agent": user_agent,
			"nkbmSourceSystem": "bnk"
		}
	
	def _request(self, method, url, params=None, data=None, headers={}, **kwargs):
		if headers:
			headers = headers.clone()
			headers.update(self._headers)
		else:
			headers = self._headers
		return self._sess.request(method, url, params=params, data=data, headers=headers, **kwargs)
	
	def _api_get(self, endpoint, *args, method="GET", **kwargs):
		resp = self._request(method, self._api_base + endpoint, *args, **kwargs)
		self._resp = resp
		if resp.status_code == 401:
			raise BNNotAuthenticatedException()
		resp.raise_for_status()
		try:
			return resp.json()
		except JSONDecodeError:
			return resp.content
		
	def save_session(self, file):
		if type(file) == str:
			file = open(file, "wb")
		pickle.dump(self._sess.cookies, file)
		if type(file) == str:
			file.close()
	
	def load_session(self, file):
		if type(file) == str:
			file = open(file, "rb")
		self._sess.cookies.update(pickle.load(file))
		if type(file) == str:
			file.close()
	
	def get_user_info(self):
		# self._request("GET", self._url_base + "/auth/prijava")
		resp = self._api_get("v1/userApi/user/info")
		return resp

	def get_customer_info(self, extended=False):
		resp = self._api_get("v1/customerApi/customers/customer" + ("Extended" if extended else ""))
		return resp

	def get_accounts(self):
		resp = self._api_get("v1/accountApi/accounts?balanceGet=true")
		return resp
	
	def get_turnovers(self, contract_id: str, date_from: datetime, date_to: datetime, rows_per_page=25, page=1, xls=False):
		"""List all account transactions in the given date range."""
		params = {
			"sortColumn": "TRANSACTION_VALUE_DATE",
			"currencyCodes": "978",
			"dateFrom": date_from.astimezone(pytz.utc).strftime("%Y-%m-%dT%H:%m:%S.000Z"),
			"dateTo": date_to.astimezone(pytz.utc).strftime("%Y-%m-%dT%H:%m:%S.000Z"),
			"pageNumber": page,
			"rowsPerPage": rows_per_page,
			"transactionDirection": "",
			"transactionStatus": "",
#			"isArchive": "true",
			"id": contract_id,
			"dateToggle": "CUSTOM",
		}
		url = f"v1/contractApi/turnovers/{contract_id}"
		if xls:
			url += "/XLSX"
		resp = self._api_get(url, params=urllib.parse.urlencode(params, safe=':+'))

		return resp
	
	def get_cards(self):
		resp = self._api_get("v1/cardApi/cards?contractActive=ALL&authorizationActive=ALL")
		return resp

	def get_card_data(self, contract_id: str):
		resp = self._api_get(f"v1/cardApi/cards/{contract_id}")
		return resp
	
	def get_orders(self, status:OrderStatus=OrderStatus.UNCONFIRMED):
		"""Returns a list of orders with the given status (default UNCONFIRMED)"""
		date_to = datetime.now()
		date_from = date_to - timedelta(days=30)
		payload = {
			"dateFrom": date_from.isoformat(),
			"dateTo": date_to.isoformat(),
			"orderStatusType": status,
			"pageNumber": 1,
			"rowsPerPage": 100,
			"sortColumn": "DATE_INSERT",
			"sortType": "DESC"
		}
		resp = self._api_get("v1/orderApi/orders/consolidated", method="POST", json=payload)
		return resp

	
	def get_flik_name(self, alias: str):
		"""Get a Flik user by their alias (usually a phone number).
		The phone number should be in international form with 00 prefix (i.e. 0038640123456)"""
		resp = bn._api_get('paymentApi/payments/alias/isRegistered/' + alias)
		return resp
	
	def send_flik_payment(self, recipient: str, amount: float, purpose:str="", account:Optional[dict]=None):
		"""Send a Flik payment
			:param recipient: Flik user alias, usually a phone number (international format with 00 prefix i.e. 0038640123456)
			:param amount: Amount to send, in EUR
			:param purpose: Message, attached to the payment
			:param account: Account to send the payment from. Use the dict from `get_accounts`. If omitted, the first account is used.
		"""
		if account is None:
			account = self.get_accounts()["result"][0]
		
		address = account["owner"]["addresses"][0]
		payload = {
			"alias": recipient,
			"purpose": purpose,
			"currency": {
				"code": 978,
				"description": "EURO",
				"name": "EUR"
			},
			"amount": amount,
			"debtor": {
				"account": {
					"account": account["contract"],
					"contractGlobalId": account["contractGlobalId"],
					"contractId": account["contractId"],
					"currency": {
						"code": 978,
						"description": "EURO",
						"name": "EUR"
					},
					"name": account["eBankInfo"]["contractName"]
				},
				"address": address["address"] + " " + address["houseNumber"],
				"city": address["postNumber"] + " " + address["city"],
				"country": {
					"countryCode": address["countryCode"],
					"countryIso": address["countryIsoCode"],
					"currency": 0,
					"name": address["country"]
				},
				"name": account["owner"]["name"]
			},
		}
	
		payload2 = self._api_get("v1/paymentApi/payments/payment/flikPayment/prepare", method="POST", json=payload)
		
		resp = self._api_get("v1/paymentApi/orders/order/flikPayment/add", method="POST", json=payload2)
		return resp


	def send_order_confirmation_otp(self, order_ids: List[str]):
		"""Send SMS OTP code to confirm the listed orders. 
		Once you get the code, call `confirm_orders` with the same `order_ids` and the OTP.
		:param order_ids: A list of order UUIDs to confirm (filed `orderId` in the response)
		"""
		payload = {
			"authenticationType": "si.nkbm.id.auth.hidsmsoob",
			"orderAction": "POTRDI",
			"orderIds": order_ids
		}
		resp = self._api_get("v1/orderApi/orders/authorize/challange", method="POST", json=payload)
		return resp

	def confirm_orders(self, order_ids: List[str], otp_code: str):
		"""Confirm orders using an OTP code. See the docs for `send_order_confirmation_otp`."""
		payload = {
			"authenticationType": "si.nkbm.id.auth.hidsmsoob",
			"authorization": otp_code,
			"orderAction": "POTRDI",
			"orderIds": order_ids
		}
		resp = self._api_get("v1/orderApi/orders/authorize", method="POST", json=payload)
		return resp

	def login(self, username, password):
		"""Log in using your username and password (6-digit PIN).
		This will send an SMS OTP to your phone and you must use `sms_otp` to enter it and finish the login process.
		"""
		data = {
			"p1": username,
			"p2": password,
			"action": "login",
			"generateChallenge": 1,
			"error": "loginAgain",
			"platform": "web",
			"version": 4.0
		}
		self._request("GET", self._url_base + "/auth/prijava")
		resp = self._request("POST", self._url_base + "/prijava/bnk", data=data)
		return resp.text
	
	def sms_otp(self, username, token):
		"""Enter the SMS OTP to finish your login (see `login`)."""
		data = {
			"p1": username,
			"p2": token,
			"action": "login",
			"authenticationType": "hsmsoob",
			"error": "loginAgain",
			"platform": "web",
			"version": 4.0
		}
		tr = self._request("GET", self._url_base)
		resp = self._request("POST", self._url_base + "/prijava/bnk", data=data)
		return resp.text
	
	def check_logged_in(self):
		try:
			self.get_user_info()
			return True
		except BNNotAuthenticatedException:
			return False
	

	
	
	
	
	
			
	
	
		
