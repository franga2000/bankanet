#!/usr/bin/env python3
from datetime import datetime, timedelta
from os import path
from sys import exit
from getpass import getpass
import json
from pathlib import Path
import threading

from bankanet import BankaNet, BNNotAuthenticatedException

SESSION_DIR = Path("local")
SESSION_FILE = str(SESSION_DIR / "bn_session.pickle")


def cli_auth(bn: BankaNet, session_file: str = SESSION_FILE):
    userinfo = None
    
    if path.exists(session_file):
        print("Found session file; loading...")
        bn.load_session(session_file)
        try:
            userinfo = bn.get_user_info()
        except BNNotAuthenticatedException:
            print("Session was invalid; re-authenticating...")

    if not userinfo:
        if not SESSION_DIR.is_dir() and os.access(str(SESSION_DIR), os.W_OK):
            print("M")
            exit(3)

        username = input("Username: ")
        password = getpass("Password: ")
        
        resp = bn.login(username, password)
        if "invalid" in resp:
            print("Prijava ni uspela:", resp)
            exit(1)
        
        resp = bn.sms_otp(username, getpass("SMS token: "))
        if "invalid" in resp:
            print("Prijava ni uspela:", resp)
            exit(2)
                
        bn.save_session(session_file)
        userinfo = bn.get_user_info()
    print("Success! You are logged in as", userinfo["username"], "-", userinfo["name"])
    write("userinfo.json", userinfo)

def write(fn, content):
    if fn.endswith(".json"):
        content = json.dumps(content)
    with open("local/" + fn, "w") as f:
        f.write(content)

def interactive(locs):
    import readline
    import code
    import rlcompleter
    variables = globals().copy()
    variables.update(locs)
    readline.set_completer(rlcompleter.Completer(variables).complete)
    readline.parse_and_bind("tab: complete")
    shell = code.InteractiveConsole(variables)
    shell.interact()

def refresh_thread(bn: BankaNet):
    import time
    while True:
        bn.get_user_info()
        time.sleep(30)

if __name__ == '__main__':
    bn = BankaNet()
    cli_auth(bn)
    
    print("ACCOUNTS:")
    accounts = bn.get_accounts()["result"]
    write("accounts.json", accounts)
    for i, account in enumerate(accounts):
        print("\t", account["contractName"] + ":")
        for balance in account["balances"]:
            print("\t\t", f'{balance["balance"]} {balance["currency"]["name"]} (avail. {balance["balanceAvailable"]}) {balance["currency"]["name"]}')
        print("\t\t", "TRANSACTIONS:")
        transactions = bn.get_turnovers(account["contractGlobalId"], datetime.now() - timedelta(days=30*6), datetime.now())
        write("turnovers_" + account["contractGlobalId"] + ".json", transactions)
        for transaction in transactions["result"]:
            print("\t\t\t\t", f"{transaction['valueDate']}\t{ '-' if 'debitAmount' in transaction else '+' }{transaction['amount']}\t{transaction['description']}")
        
    print("CARDS:")
    cards = bn.get_cards()["result"]
    write("cards.json", cards)
    for i, card in enumerate(cards):
        print("\t", card["contractName"] + ":")
        print("\t\t", f'{card["balance"]} {card["currency"]["name"]} (avail. {card["availableBalance"]}) {card["currency"]["name"]}')
        card_data = bn.get_card_data(card["contractGlobalId"])
        write("card_data_" + card["contractGlobalId"] + ".json", card_data)

    threading.Thread(target=refresh_thread, args=(bn,), daemon=True).start()
    interactive(locals())
