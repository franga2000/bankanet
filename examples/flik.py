from bankanet import BankaNet
from bankanet.cli import cli_auth

bn = BankaNet()
cli_auth(bn, session_file="/tmp/bankanet_session.pickle")

order = bn.send_flik_payment(
    recipient=input("Recipient number: "), 
    amount=float(input("Amount: ")), 
    purpose=input("Purpose: "),
)

print("Created order ID: ", order["orderId"])

bn.send_order_confirmation_otp([order["orderId"]])

otp = input("SMS OTP: ")
bn.confirm_orders([order["orderId"]], otp)
