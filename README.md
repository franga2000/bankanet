# Unofficial Bank@Net API

## ⚠️ WARNING:

**This is completely unofficial, most likely against the terms of service and can pose a huge security threat if used incorrectly!** Use this only if you know what you're doing and entirely **AT YOUR OWN RISK!**

## What is it

**TL;DR:** it's an unofficial Python library, as well as a corresponding CLI utility to interact with the Bank@Net e-banking platform of Nova KBM. In its current state, it only supports looking up your account balance, and as that is the only thing I use it for, but you are free to add support for more if you require it.

## How does it work?

It uses the exact same APIs as the normal web app you would use in a browser does. It implements the standard Username+PIN+SMS authentication flow and provides you with a simple CLI utility that guides you through it. The session, resulting from successful authentication, can then be stored on disk and used potentially indefinitely, as long as a request to the API is made often enough so the session is renewed (5 minutes).

## How do I use it?

After installing it, first import and initialize the connector:

```python
from bankanet import BankaNet
bn = BankaNet()
```

Next, you will need to somehow authenticate yourself. The easiest way to do this is using the included CLI authenticator, which will attempt to read an existing session from the given file (2nd parameter) and if it is not valid or missing, give you an authentication prompt and save the resulting session.

```python
from bankanet.cli import cli_auth
cli_auth(bn, "bn_session.pickle")
```

You can now interact with your accounts:

```python
accounts = bn.get_accounts()["result"]
for i, account in enumerate(accounts):
    print("\t", account["contractName"] + ":")
    for balance in account["balances"]:
        print("\t\t", str(balance["balance"]) + " " + balance["currency"]["name"], "(avail.", str(balance["balanceAvailable"]) + " " + balance["currency"]["name"] + ")")
```

### Advanced

If you want, you can also split the authentication from your main application by replacing the `cli_auth` call with `bn.load_session` and running the authentication manually in a different script. A custom authenticaton flow can also be written, by directly calling the `login` and `sms_otp` methods and getting user input however makes sense for your application. See the source of `cli_auth` for inspiration.

A list of API endpoints, extracted from the mobile app, can be found in [`API.txt`](API.txt). You can call them using the private method `_api_get` or `_request`.