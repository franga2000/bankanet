#!/usr/bin/env python

from distutils.core import setup

setup(name='bankanet',
      version='2.0',
      description='Unofficial Bank@Net API',
      author='Miha Frangež',
      author_email='miha.frangez@gmail.com',
      packages=['bankanet'],
      requires=['pytz','requests'],
)
